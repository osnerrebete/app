import React from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  Row,
  Col,
  FormGroup,
  Label,
  Input
} from "reactstrap";
import CircleLoader from "react-spinners/CircleLoader";

const api = "http://localhost:4000";

class ViewDetail extends React.Component {
  state = {
    modal: false,
    title: "",
    class: "",
    coordinate: "",
    loading: false,
    loaded: false,
    items: null,
    plate: ""
  };

  toggle = () => this.setState({ modal: !this.state.modal });

  handleChange = ({ target }: any) => {
    const { value, name } = target;
    this.setState({
      [name]: value
    });
  };

  onClick = () => {
    this.setState({
      loading: true,
      loaded: false,
      items: null
    });
    const data = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        coordinate: this.state.coordinate,
        plate: this.state.plate
      })
    };
    fetch(`${api}/search_coordinates`, data)
      .then(response => response.json())
      .then(
        result => {
          this.setState({
            loading: false,
            loaded: true,
            items: result
          });
        },
        error => {
          console.dir(error);
        }
      );
  };

  emptyForm(): boolean {
    return this.state.coordinate === "";
  }

  render() {
    let results;
    const {loading, loaded, items = []} = this.state;
    if (loading)
      results = (
        <Col>
          <Row>
            <Col className="loader">
              <CircleLoader size={50} />
            </Col>
          </Row>
          <h2>Cargando resultados</h2>
        </Col>
      );
    if (!loaded || items?.length === 0) results = null;
    if (!loading && items) {
      results = items.map(
        ({
          id,
          status
        }: {
          id: string;
          plate: string;
          lonlat: string;
          current_date: string;
          lapse: number;
          status: string;
        }) => (
          <span key={id}>
            {status}
            </span>
        )
      );
      

    } else {
      results = null;
    }

    return (
      <div>
        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.state.class}
          size="lg"
        >
          <ModalHeader toggle={this.toggle}>{this.state.title}</ModalHeader>
          <ModalBody>
            <Form>
              <Row>
                <Col>
                  <FormGroup>
                    <Label>Buscar coordenada</Label>
                    <Input
                      type="text"
                      name="coordinate"
                      placeholder="Ej. -70.6907,-33.4538"
                      onChange={e => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
                <Col>
                  <Button
                    color="primary"
                    type="button"
                    onClick={this.onClick}
                    className="search"
                    disabled={this.emptyForm()}
                  >
                    Buscar
                  </Button>
                </Col>
              </Row>
            </Form>
            <hr />
            {results}
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default ViewDetail;
