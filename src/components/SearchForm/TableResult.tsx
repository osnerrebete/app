import React from "react";
import { Button, Col, Jumbotron, Table } from "reactstrap";
import ViewDetail from "./ViewDetail";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";

interface Props {
  items: any;
}

interface State {
  items: any;
}

class TableResult extends React.Component<Props, State> {
  private viewDetailRef = React.createRef<ViewDetail>();

  constructor(props: any) {
    super(props);
    this.state = {
      items: this.props.items
    };
    this.openModal = this.openModal.bind(this);
  }

  updateProps = () => {
    this.setState({
      items: this.props.items
    });
  };

  openModal(title: any, className: any, plate: string) {
    if (this.viewDetailRef.current) {
      this.viewDetailRef.current.setState({ title: title, class: className, plate: plate });
      this.viewDetailRef.current.toggle();
    }
  }

  render() {
    let items = this.state.items;
    let rows;
    if (items && items.length > 0) {
      rows = items.map(
        ({
          id,
          plate,
          start_date,
          end_date,
          status
        }: {
          id: string;
          plate: string;
          start_date: string;
          end_date: string;
          status: string;
        }) => (
          <tr key={id}>
            <td>{plate}</td>
            <td>{start_date}</td>
            <td>{end_date}</td>
            <td>{status}</td>
            <td>
              <Button
                color="link"
                onClick={() => {
                  this.openModal(
                    `Vista de Detalle Bus con Patente: ${plate}`,
                    `detalle${id}`, plate
                  );
                }}
              >
                <FontAwesomeIcon icon={faEye} /> Ver detalle
              </Button>
            </td>
          </tr>
        )
      );
    } else {
      rows = (
        <tr>
          <td colSpan={5}>No se encontraron registros</td>
        </tr>
      );
    }

    return (
      <Jumbotron>
        <Col>
          <h2>Resultados:</h2>
          <Table bordered>
            <thead>
              <tr>
                <th>Patente</th>
                <th>Hora de salida</th>
                <th>Hora de llegada</th>
                <th>Estado</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>{rows}</tbody>
          </Table>
        </Col>
        <ViewDetail
          ref={this.viewDetailRef}
        />
      </Jumbotron>
    );
  }
}

export default TableResult;
