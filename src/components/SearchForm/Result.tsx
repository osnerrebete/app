import React from "react";
import { Row, Col, Jumbotron } from "reactstrap";
import CircleLoader from "react-spinners/CircleLoader";
import TableResult from "./TableResult";

interface Props {
  loading: boolean;
  loaded: boolean;
  items: any;
}
interface State {
  loading: boolean;
  loaded: boolean;
  items: any;
}

class Result extends React.Component<Props, State> {
  private tableResult = React.createRef<TableResult>();
  state = {
    loading: this.props.loading,
    loaded: this.props.loaded,
    items: this.props.items,
  };
  updateProps = () => {
    this.setState({
      loading: this.props.loading,
      loaded: this.props.loaded,
      items: this.props.items,
    });
    if (this.tableResult.current) {
      this.tableResult.current.updateProps();
    }
  };
  render() {
    const { loading, loaded, items } = this.props;
    if (loading)
      return (
        <Jumbotron className="loading">
          <Col>
            <Row>
              <Col className="loader">
                <CircleLoader size={50} />
              </Col>
            </Row>
            <h2>Cargando resultados</h2>
          </Col>
        </Jumbotron>
      );
    if (!loaded || !items) return null;
    if (!this.state.loading && this.state.items !== null) {
      return <TableResult items={this.state.items} ref={this.tableResult}/>;
    } else {
      return null;
    }
  }
}

export default Result;
