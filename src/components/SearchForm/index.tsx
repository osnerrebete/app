import React from "react";
import Search from "./Search";
import { Row } from "reactstrap";
import "./style.css";

const SearchForm = () => {
  return (
    <Row>
      <Search />
    </Row>
  );
};

export default SearchForm;
