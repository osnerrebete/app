import React from "react";
import {
  Row,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  Col,
  Jumbotron
} from "reactstrap";
import DatePicker from "react-date-picker";
import Result from "./Result";

const api = "http://localhost:4000";

class Search extends React.Component {
  private resultRef = React.createRef<Result>();
  state = {
    route: "",
    plate: "",
    date: undefined,
    error: null,
    loading: false,
    loaded: false,
    items: null,
    routeList: []
  };

  onClick = () => {
    this.setState({
      loading: true,
      loaded: false,
      items: null
    });
    const data = {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({plate: this.state.plate, route_id: this.state.route, date: this.state.date})
    }
    fetch(`${api}/search`, data)
      .then(response => response.json())
      .then(
        result => {
          this.setState({
            loading: false,
            loaded: true,
            items: result
          });
          if (this.resultRef.current) {
            this.resultRef.current.updateProps();
          }
        },
        error => {
          console.dir(error);
        }
      );
  };

  componentDidMount() {
    let routeList = [];
    fetch(`${api}/routes`)
      .then(response => response.json())
      .then((data: any) => {
        routeList = data;
        this.setState({
          routeList: routeList
        });
      });
  }

  selectOptions() {
    let routeList = this.state.routeList;
    let optionItems = routeList.map(
      ({
        id,
        start_name,
        end_name
      }: {
        id: number;
        start_name: string;
        end_name: string;
      }) => (
        <option key={id} value={id}>
          {start_name} => {end_name}
        </option>
      )
    );
    return optionItems;
  }

  emptyForm(): boolean {
    return (
      this.state.route === "" ||
      this.state.date === undefined ||
      this.state.date === null
    );
  }

  handleChange = ({ target }: any) => {
    const { value, name } = target;
    this.setState({
      [name]: value
    });
  };

  onDateChange = (date: any) => this.setState({ date });

  render() {
    return (
      <Col>
        <Jumbotron>
          <Col>
            <h1>¿Cuál es tu próximo destino?</h1>
            <Form>
              <Row>
                <Col>
                  <FormGroup>
                    <Label>Ruta</Label>
                    <Input
                      type="select"
                      name="route"
                      onChange={e => this.handleChange(e)}
                    >
                      <option value="">Selecciona la ruta</option>
                      {this.selectOptions()}
                    </Input>
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup>
                    <Label>Fecha de salida</Label>
                    <DatePicker
                      onChange={this.onDateChange}
                      value={this.state.date}
                      name="date"
                    />
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup>
                    <Label>Patente del bus (opcional)</Label>
                    <Input
                      type="text"
                      name="plate"
                      placeholder="Ej. XXYY12"
                      onChange={e => this.handleChange(e)}
                    />
                  </FormGroup>
                </Col>
                <Col xs={1}>
                  <Button
                    color="primary"
                    type="button"
                    onClick={this.onClick}
                    className="search"
                    disabled={this.emptyForm()}
                  >
                    Buscar
                  </Button>
                </Col>
              </Row>
            </Form>
          </Col>
        </Jumbotron>
        <Result
          loaded={this.state.loaded}
          loading={this.state.loading}
          items={this.state.items}
          ref={this.resultRef}
        />
      </Col>
    );
  }
}

export default Search;
