import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCloud } from '@fortawesome/free-solid-svg-icons';


const Info = ({humidity, wind}: {humidity: number, wind: string})  => {
return(<div><span><FontAwesomeIcon icon={faCloud} /> {`${humidity}% - `}</span><span>{`${wind} wind`}</span></div>)
};

export default Info;