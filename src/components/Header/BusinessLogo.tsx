import React from "react";
import { Col, CardImg } from "reactstrap";
import Logo from "./../../assests/logo.png";

const BusinessLogo = () => {
  return (
    <Col className="text-center">
      <CardImg src={Logo} className="logo" />
    </Col>
  );
};

export default BusinessLogo;
