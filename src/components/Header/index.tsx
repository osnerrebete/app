import React from "react";
import BusinessLogo from "./BusinessLogo";
import { Row } from "reactstrap";
import "./style.css";

const Header = () => {
  return (
    <Row>
      <BusinessLogo />
    </Row>
  );
};

export default Header;
