import React from "react";
import { Container } from "reactstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import SearchForm from "./components/SearchForm";
import Header from "./components/Header";

function App() {
  return (
    <Container>
      <Header />
      <SearchForm />
    </Container>
  );
}

export default App;
