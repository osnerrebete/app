## Descripción
El sistema está dividido en dos partes app (frontend) y api (backend):
- APP: está hecho en React y tiene dos componentes principales el header y el searchform (éste contiene el formulario de búsqueda, la tabla de resultados y el modal que permite buscar rutas usando coordenadas). El objetivo era crear una single-page application, es decir, una página única dónde se pudieran realizar la mayoría de las cosas
- API: está hecho en Rails 5, cree 4 controladores principales: 
    - gps_coordinates: aquí se tiene el endpoint que recibe datos del GPS y hace uso del modelo del mismo nombre para almacenar los datos
    - routes: lista todas las rutas que se tienen actualmente, comentaron que podían crecer en el tiempo así que cree una relación muchos a muchos con los buses, de manera que se puedan agregar en el futuro, asumí que las rutas ida y vuelta no necesariamente son iguales así que cree dos por defecto
    - search_buses: cree vistas para colocar allí las consultas complejas y usé la gema scenic para gestionar los archivos SQL, de manera que luego cree un modelo asociado a esas vistas y un controlador. Este controlador permite buscar los viajes de los buses enviando el id de la ruta, la fecha del viaje y la patente (opcional), y retorna los resultados correspondientes.
    - search_coordinates: este es similar al controlador anterior, la diferencia es que recibe la patente y la coordenada a buscar y retorna si estuvo allí, si se detuvo y si no pasó por esa coordenada.

## Cosas que me hubiese gustado mejorar/hacer:
- Me hubiese gustado probar más la búsqueda por coordenadas (search_coordinates) y hacer las mejoras pertinentes a la consulta
- Mejorar el diseño del front y hacer validaciones de los inputs en términos de usabilidad para prevenir los errores
- Realizar los tests correspondientes para back (hubiese utilizado Rspec para hacer las pruebas correspondientes) y front
- Realizar la funcionalidad de listar los eventos de salida/entrada de ruta
- Separar mejor los componentes de react para poder reutilizarlos en el futuro

## Instrucciones de Ejecución:
## Scripts disponibles

En la carpeta del proyecto para instalar las dependencias, por favor emplea el siguiente comando:
### `npm install`

Una vez finalizada la instalación puedes emplear el comando:
### `npm start`

Esto hará que app corra en el modo development, y podrás acceder automáticamente en el navegador a través de la siguiente URL:
[http://localhost:3000](http://localhost:3000) 
